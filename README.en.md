# 基于LSTM时间序列预测（简单又好用）无脑代码，使用很简单，跟着注释和使用手册用就行.

#### Description
基于LSTM时间序列预测Python程序
简介：
      1、单变量，多变量输入，自由切换 
      2、单步预测，多步预测，自动切换
      3、基于Pytorch架构，单输出
      4、多个评估指标（MAE,MSE,R2,MAPE等）
      5、数据从excel/csv文件中读取，更换简单
      6、标准框架，数据分为训练集、验证集，测试集


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
